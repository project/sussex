<?php

/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can add new regions for block content, modify
 *   or override Drupal's theme functions, intercept or make additional
 *   variables available to your theme, and create custom PHP logic. For more
 *   information, please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to sussex_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: sussex_breadcrumb()
 *
 *   where sussex is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override any of the theme functions used in Zen core,
 *   you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_item_link()   in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions, please visit the Theme
 *   Developer's Guide on Drupal.org: http://drupal.org/node/223430
 *   For more information on template suggestions, please visit the Theme
 *   Developer's Guide on Drupal.org: http://drupal.org/node/223440 and
 *   http://drupal.org/node/190815#template-suggestions
 */


/*
 * Add any conditional stylesheets you will need for this sub-theme.
 *
 * To add stylesheets that ALWAYS need to be included, you should add them to
 * your .info file instead. Only use this section if you are including
 * stylesheets based on certain conditions.
 */
/* -- Delete this line if you want to use and modify this code
// Example: optionally add a fixed width CSS file.
if (theme_get_setting('sussex_fixed')) {
  drupal_add_css(path_to_theme() . '/layout-fixed.css', 'theme', 'all');
}
// */


/**
 * Implementation of HOOK_theme().
 */
function sussex_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here );
  */
  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}


/**
 *  Implements hook_color() of the next generation Color module.
 */
function sussex_color() {
    $color = array();

    $color['theme'] = 'sussex';

    $color['replacement methods'] = array('tag' => TRUE);

    $color['fields'] = array(
      'link',
      'text',
      'container-background',
      'body-background-top',
      'body-background-bottom',
      'logo-top',
      'logo-bottom',
      'logo-border',
      'footer-border',
      'footer-top',
      'footer-bottom',
      'footer-text',
      'frame-outer',
      'frame-inner',
      'sidebar-background',
      'menu-top',
      'menu-bottom',
      'menu-text',
      'menu-hover',
      'menu-hover-text',
      'submenu-frame',
      'submenu-background',
      'submenu-text',
      'submenu-hover',
      'submenu-hover-text',
    );

    $color['premade schemes'] = array();
    $color['premade schemes']['Default'] = array (
        'link' => '#1e2a4d',
        'text' => '#454545',
        'container-background' => '#ffffff',
        'body-background-top' => '#c8c8c8',
        'body-background-bottom' => '#e9e9e9',
        'logo-top' => '#f5f5f5',
        'logo-bottom' => '#ffffff',
        'logo-border' => '#777777',
        'footer-border' => '#777777',
        'footer-top' => '#343a4b',
        'footer-bottom' => '#3e465a',
        'footer-text' => '#f9f9f9',
        'frame-outer' => '#333333',
        'frame-inner' => '#bbbbbb',
        'sidebar-background' => '#eeeeee',
        'menu-top' => '#343a4b',
        'menu-bottom' => '#3e465a',
        'menu-text' => '#ffffff',
        'menu-hover' => '#777777',
        'menu-hover-text' => '#ffffff',
        'submenu-frame' => '#343a4b',
        'submenu-background' => '#222222',
        'submenu-text' => '#ffffff',
        'submenu-hover' => '#777777',
        'submenu-hover-text' => '#ffffff',
    );
    $color['premade schemes']['GreenClip'] = array (
        'link' => '#556835',
        'text' => '#454545',
        'container-background' => '#ffffff',
        'body-background-top' => '#dbdbdb',
        'body-background-bottom' => '#c2c2c2',
        'logo-top' => '#f5f5f5',
        'logo-bottom' => '#ffffff',
        'logo-border' => '#111111',
        'footer-border' => '#111111',
        'footer-top' => '#718a47',
        'footer-bottom' => '#63793e',
        'footer-text' => '#fcfcfc',
        'frame-outer' => '#63793e',
        'frame-inner' => '#bbbbbb',
        'sidebar-background' => '#eeeeee',
        'menu-top' => '#556835',
        'menu-bottom' => '#63793e',
        'menu-text' => '#ffffff',
        'menu-hover' => '#111111',
        'menu-hover-text' => '#ffffff',
        'submenu-frame' => '#666666',
        'submenu-background' => '#f9f9f9',
        'submenu-text' => '#333333',
        'submenu-hover' => '#333333',
        'submenu-hover-text' => '#ffffff',
    );
    $color['premade schemes']['Syspiro'] = array (
        'link' => '#201e66',
        'text' => '#333333',
        'container-background' => '#fafafa',
        'body-background-top' => '#dedede',
        'body-background-bottom' => '#b3bdc4',
        'logo-top' => '#f0f0f0',
        'logo-bottom' => '#fafafa',
        'logo-border' => '#98c1d4',
        'footer-border' => '#98c1d4',
        'footer-top' => '#201e66',
        'footer-bottom' => '#1a1852',
        'footer-text' => '#f3f3f3',
        'frame-outer' => '#777777',
        'frame-inner' => '#aaaaaa',
        'sidebar-background' => '#f5f5f5',
        'menu-top' => '#201e66',
        'menu-bottom' => '#26247a',
        'menu-text' => '#ffffff',
        'menu-hover' => '#e0922f',
        'menu-hover-text' => '#ffffff',
        'submenu-frame' => '#98c1d4',
        'submenu-background' => '#f5f5f5',
        'submenu-text' => '#222222',
        'submenu-hover' => '#fab43f',
        'submenu-hover-text' => '#ffffff',
    );
    $color['premade schemes']['Edoctrina'] = array (
        'link' => '#a26415',
        'text' => '#454545',
        'container-background' => '#ffffff',
        'body-background-top' => '#777777',
        'body-background-bottom' => '#999999',
        'logo-top' => '#f5f5f5',
        'logo-bottom' => '#ffffff',
        'logo-border' => '#e0922f',
        'footer-border' => '#e0922f',
        'footer-top' => '#3b3b3d',
        'footer-bottom' => '#333333',
        'footer-text' => '#f9f9f9',
        'frame-outer' => '#333333',
        'frame-inner' => '#bbbbbb',
        'sidebar-background' => '#F8F8F8',
        'menu-top' => '#6d6e71',
        'menu-bottom' => '#222222',
        'menu-text' => '#ffffff',
        'menu-hover' => '#e0922f',
        'menu-hover-text' => '#ffffff',
        'submenu-frame' => '#222222',
        'submenu-background' => '#6d6e71',
        'submenu-text' => '#ffffff',
        'submenu-hover' => '#e0922f',
        'submenu-hover-text' => '#ffffff',
    );
    
    $color['default scheme'] = 'Default';

    $color['reference scheme'] = 'Default'; // just for kicks, you may not need but be safe.

    $color['stylesheets'] = array('colors.css');

    $color['blend target'] = '#ffffff';

    $color['images']['logo-background'] = array(
      'file' => 'images/blank-1x1000.png',
      'fill' => array(
        array(
          'type'   => 'y-gradient',
          'x'      => 0,
          'y'      => 0,
          'width'  => 1,
          'height' => 50,
          'colors' => array('logo-top', 'logo-bottom'),
        ),
        array(
          'type'   => 'solid',
          'x'      => 0,
          'y'      => 50,
          'width'  => 1,
          'height' => 950,
          'colors' => array('logo-bottom'),
        ),
      ),
      'slices' => array(
        'images/logo-bg.png' => array(
          'x'      => 0,
          'y'      => 0,
          'width'  => 1,
          'height' => 1000,
        ),
      ),
    );

    $color['images']['footer'] = array(
      'file' => 'images/blank-1x1000.png',
      'fill' => array(
        array(
          'type'   => 'solid',
          'x'      => 0,
          'y'      => 0,
          'width'  => 1,
          'height' => 950,
          'colors' => array('footer-top'),
        ),    
        array(
          'type'   => 'y-gradient',
          'x'      => 0,
          'y'      => 950,
          'width'  => 1,
          'height' => 50,
          'colors' => array('footer-top', 'footer-bottom'),
        ),
      ),
      'slices' => array(
        'images/footer-bg.png' => array(
          'x'      => 0,
          'y'      => 0,
          'width'  => 1,
          'height' => 1000,
        ),
      ),
    );

    $color['images']['body-background'] = array(
      'file' => 'images/blank-1x1000.png',
      'fill' => array(
        array(
          'type'   => 'y-gradient',
          'x'      => 0,
          'y'      => 0,
          'width'  => 1,
          'height' => 600,
          'colors' => array('body-background-top', 'body-background-bottom'),
        ),
        array(
          'type'   => 'solid',
          'x'      => 0,
          'y'      => 600,
          'width'  => 1,
          'height' => 400,
          'colors' => array('body-background-bottom'),
        ),
      ),
      'slices' => array(
        'images/body-bg.png' => array(
          'x'      => 0,
          'y'      => 0,
          'width'  => 1,
          'height' => 1000,
        ),
      ),
    );

    $color['images']['menu-background'] = array(
      'file' => 'images/blank-1x1000.png',
      'fill' => array(
        array(
          'type'   => 'solid',
          'x'      => 0,
          'y'      => 0,
          'width'  => 1,
          'height' => 970,
          'colors' => array('menu-top'),
        ),
        array(
          'type'   => 'y-gradient',
          'x'      => 0,
          'y'      => 970,
          'width'  => 1,
          'height' => 30,
          'colors' => array('menu-top', 'menu-bottom'),
        ),
      ),
      'slices' => array(
        'images/menu-bg.png' => array(
          'x'      => 0,
          'y'      => 0,
          'width'  => 1,
          'height' => 1000,
        ),
      ),
    );

    $color['images']['menu-hover'] = array(
      'file' => 'images/blank-10x10.png',
      'fill' => array(
        array(
          'type'   => 'solid',
          'x'      => 0,
          'y'      => 0,
          'width'  => 10,
          'height' => 10,
          'colors' => array('menu-hover'),
        ),
      ),
      'slices' => array(
        'images/menu-hover.png' => array(
          'x'      => 0,
          'y'      => 0,
          'width'  => 10,
          'height' => 10,
        ),
      ),
    );

    $color['images']['submenu-hover'] = array(
      'file' => 'images/blank-10x10.png',
      'fill' => array(
        array(
          'type'   => 'solid',
          'x'      => 0,
          'y'      => 0,
          'width'  => 10,
          'height' => 10,
          'colors' => array('submenu-hover'),
        ),
      ),
      'slices' => array(
        'images/submenu-hover.png' => array(
          'x'      => 0,
          'y'      => 0,
          'width'  => 10,
          'height' => 10,
        ),
      ),
    );

    $color['images']['submenu-background'] = array(
      'file' => 'images/blank-10x10.png',
      'fill' => array(
        array(
          'type'   => 'solid',
          'x'      => 0,
          'y'      => 0,
          'width'  => 10,
          'height' => 10,
          'colors' => array('submenu-background'),
        ),
      ),
      'slices' => array(
        'images/submenu-bg.png' => array(
          'x'      => 0,
          'y'      => 0,
          'width'  => 10,
          'height' => 10,
        ),
      ),
    );

    $color['images']['header-border-bottom'] = array(
      'file' => 'images/shadows.png',
      'fill' => array(
        array(
          'type'   => 'solid',
          'x'      => 0,
          'y'      => 0,
          'width'  => 110,
          'height' => 110,
          'colors' => array('container-background'),
        ),
      ),
      'slices' => array(
        'images/container-bg.png' => array(
          'x'      => 10,
          'y'      => 105,
          'width'  => 10,
          'height' => 5,
        ),
      ),
    );

    return $color;
}
/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function sussex_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function sussex_preprocess_page(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function sussex_preprocess_node(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function sussex_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function sussex_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */
